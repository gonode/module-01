const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const path = require('path');
const moment = require('moment');

nunjucks.configure('views', {
    autoescape: true,
    express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: false }));

const ageMiddleware = (req, res, next) => {
    const { username } = req.query;
    if (username.trim().length === 0) {
        return res.redirect('/');
    }
    return next();
};

app.get('/', (req, res) => {
    return res.render('main');
});

app.post('/check', (req, res) => {
    const { username, birthdate } = req.body;
    const AGE = 18;
    const ageUser = moment().diff(birthdate, 'years', false);

    if (birthdate) {
        if (ageUser > AGE) {
            return res.redirect(`/major?username=${username}`);
        }
        return res.redirect(`/minor?username=${username}`);
    }
    return res.redirect('/');
});

app.get('/major', ageMiddleware, (req, res) => {
    const { username } = req.query;
    res.render('major', { username });
});

app.get('/minor', ageMiddleware, (req, res) => {
    const { username } = req.query;
    res.render('minor', { username });
});

app.listen(3000);
